Exchange rate Demo Application
========================

The "Exchange rate  Demo Application" is a reference application created to show Exchange rate.

Requirements
------------

  * PHP 7.1.3 or higher;
  * PDO-MySQL PHP extension enabled;
  * and the [usual Symfony application requirements][2].

Installation
------------

Clone or download repository
```sh
   git clone git@bitbucket.org:lokesh_yadav/exchange-rate.git
```
Run composer

```sh
$ composer install
```
Setup Environment
```sh
$ cp .env.test .env
```
Database connection in .env
```sh
DATABASE_URL=mysql://user_name:password@127.0.0.1:3306/database_name
```

Upload Database tables

```sh
$ php bin/console doctrine:schema:update --force
```

Usage
-----

Get Latest Exchange Rate for "USD" to IND and EUR by executing following command
```sh
$ php bin/console rate:get-exchange-rate
```
Able to view all rates (currency, date/time, rate, last updated date/time) on route
```sh
http://website.com/rates/
```
  
Able to add/edit/delete a rate manually through the web interface
```sh
http://website.com/admin/rates/
```

If you don't have the Symfony client installed, run `php bin/console server:run`.
Alternatively, you can [configure a web server][3] like Nginx or Apache to run
the application.

Tests
-----

Execute this command to run tests:

```bash
$ cd my_project/
$ php bin/phpunit
```

[1]: https://symfony.com/doc/current/best_practices/index.html
[2]: https://symfony.com/doc/current/reference/requirements.html
[3]: https://symfony.com/doc/current/cookbook/configuration/web_server_configuration.html