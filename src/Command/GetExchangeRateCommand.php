<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use App\Helper\BasicHelper;
use App\Entity\ExchangeRate;

class GetExchangeRateCommand extends Command
{

    //Name of the command (the part after "bin/console")
    protected static $defaultName = 'rate:get-exchange-rate';

    //Get exchange rate for
    protected static $exchangeRateApi = 'https://api.exchangeratesapi.io/latest?base=USD&symbols=INR,EUR';

    /**
     * Set the console command config.
     *
     * @param void
     * @return void
     */
    protected function configure()
    {
        $this->setName('rate:get-exchange-rate')
                ->setDescription('Populate table exchange with exchange for diff currencies wrt Euro.')
                ->setHelp('For call this command enter php app/console rate:get-exchange-rate');
    }

    /**
     * Executed when command is called.
     *
     * @param object $input
     * @param object $output
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $httpClient = HttpClient::create();
            $response = $httpClient->request('GET', self::$exchangeRateApi, array());

            //Get response related details
            $statusCode = $response->getStatusCode();
            $contentType = '';
            $requestHeader = $response->getHeaders();
            $contentType = $requestHeader['content-type'][0] ?? '';
            
            //In case request was succesfull
            if ($statusCode == 200 && $contentType == 'application/json') {
                //Convert to array
                $content = $response->getContent();
                if (!empty($content)) {
                    $content = json_decode($content, true);
                }

                if (!empty($content['date']) && strtotime($content['date']) && !empty($content['base']) && !empty($content['rates'])) {
                    $lastUpdateDate = BasicHelper::setDbDate($content['date']);
                    $baseCurrency = $content['base'];

                    $entityManager = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();

                    $i = 0;
                    foreach ($content['rates'] as $currency => $rate) {
                        $exchangeRate = new ExchangeRate();

                        $exchangeRate->setBaseCurrency($baseCurrency);
                        $exchangeRate->setCurrency($currency);
                        $exchangeRate->setRate(round($rate, 2));
                        $exchangeRate->setLastUpdated(new \DateTime($lastUpdateDate));

                        //Persist $exchangerate object
                        $entityManager->persist($exchangeRate);
                        $i++;
                    }

                    //Execute query
                    $entityManager->flush();

                    //Output on console
                    $output->writeln("Total $i record inserted.");
                }
            }
        } catch (\Exception $e) {
            //Output on console
            $output->writeln("Some Error occurred.");
        }
    }
}
