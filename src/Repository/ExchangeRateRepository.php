<?php

namespace App\Repository;

use App\Entity\ExchangeRate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ExchangeRate|null find($id, $lockMode = null, $lockVersion = null)
 * @method ExchangeRate|null findOneBy(array $criteria, array $orderBy = null)
 * @method ExchangeRate[]    findAll()
 * @method ExchangeRate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExchangeRateRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ExchangeRate::class);
    }

    /**
     * Save Exchange rate.
     *
     * @param array $data <multi dimensional array>
     * @return object exchangeRate
     * @author Daffodil Software
     */
    public function saveMultipleExchangeRate($datas)
    {
        $entityManager = $this->getDoctrine()->getManager();

        foreach ($datas as $data) {
            $exchangeRate = new ExchangeRate($data);

            //Persist $exchangerate object
            $entityManager->persist($exchangeRate);
        }
//        $exchangeRate = new ExchangeRate();
//        $exchangeRate->setBaseCurrency($data['']);
//        $exchangeRate->setCurrency();
//        $exchangeRate->setRate();
//        $exchangeRate->setLastUpdated();
//        $exchangeRate->setCreatedAt();
//        $exchangeRate->setUpdatedAt();
        //Execute query
        $entityManager->flush();
    }

    // /**
    //  * @return ExchangeRate[] Returns an array of ExchangeRate objects
    //  */
    /*
      public function findByExampleField($value)
      {
      return $this->createQueryBuilder('e')
      ->andWhere('e.exampleField = :val')
      ->setParameter('val', $value)
      ->orderBy('e.id', 'ASC')
      ->setMaxResults(10)
      ->getQuery()
      ->getResult()
      ;
      }
     */

    /*
      public function findOneBySomeField($value): ?ExchangeRate
      {
      return $this->createQueryBuilder('e')
      ->andWhere('e.exampleField = :val')
      ->setParameter('val', $value)
      ->getQuery()
      ->getOneOrNullResult()
      ;
      }
     */
}
