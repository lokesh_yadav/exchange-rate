<?php

namespace App\Controller\Frontend;

use App\Repository\ExchangeRateRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rates")
 */
class ExchangeRateController extends AbstractController
{
    /**
     * @Route("/", name="exchange_rate_index", methods={"GET"})
     */
    public function index(ExchangeRateRepository $exchangeRateRepository): Response
    {
        return $this->render('frontend/rates.html.twig', [
            'exchange_rates' => $exchangeRateRepository->findAll(),
        ]);
    }
}
