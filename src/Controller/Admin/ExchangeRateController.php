<?php

namespace App\Controller\Admin;

use App\Entity\ExchangeRate;
use App\Form\ExchangeRateType;
use App\Repository\ExchangeRateRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/rates")
 */
class ExchangeRateController extends AbstractController
{
    /**
     * @Route("/", name="admin_exchange_rate_index", methods={"GET"})
     */
    public function index(ExchangeRateRepository $exchangeRateRepository): Response
    {
        return $this->render('admin/exchange_rate/index.html.twig', [
            'exchange_rates' => $exchangeRateRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="admin_exchange_rate_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $exchangeRate = new ExchangeRate();
        $form = $this->createForm(ExchangeRateType::class, $exchangeRate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($exchangeRate);
            $entityManager->flush();

            return $this->redirectToRoute('admin_exchange_rate_index');
        }

        return $this->render('admin/exchange_rate/new.html.twig', [
            'exchange_rate' => $exchangeRate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_exchange_rate_show", methods={"GET"})
     */
    public function show(ExchangeRate $exchangeRate): Response
    {
        return $this->render('admin/exchange_rate/show.html.twig', [
            'exchange_rate' => $exchangeRate,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="admin_exchange_rate_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ExchangeRate $exchangeRate): Response
    {
        $form = $this->createForm(ExchangeRateType::class, $exchangeRate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_exchange_rate_index');
        }

        return $this->render('admin/exchange_rate/edit.html.twig', [
            'exchange_rate' => $exchangeRate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="admin_exchange_rate_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ExchangeRate $exchangeRate): Response
    {
        if ($this->isCsrfTokenValid('delete'.$exchangeRate->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($exchangeRate);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_exchange_rate_index');
    }
}
