<?php

namespace App\Helper;

class BasicHelper
{
    public static $dbDateTimeFormat = 'Y-m-d H:i:s';
    public static $dbDateFormat = 'Y-m-d';
    public static $dbTimeFormat = 'H:i:s';
    public static $userDateFormat = 'm/d/Y';
    public static $formDateFormat = 'm/d/Y';
    public static $userDateTimeFormat = 'j M, Y, g:i A';

    /**
     * This function will be used to set Date format in Mysql server format.
     *
     * @param date $date
     * @return date $date
     * @author Daffodil
     */
    public static function setDbDateTime($date)
    {
        //Changing date format and return 0 incase og wrong date
        if (strtotime($date)) {
            $date = date(self::$dbDateTimeFormat, strtotime($date));
        } else {
            $date = '';
        }

        return $date;
    }

    /**
     * This function will be used to set Date format in MySql server format.
     *
     * @param date $date
     * @return date $date
     * @author Daffodil
     */
    public static function setDbDate($date)
    {
        //Changing date format and return 0 incase of wrong date
        if (strtotime($date)) {
            $date = date(self::$dbDateFormat, strtotime($date));
        } else {
            $date = '';
        }

        return $date;
    }

    /**
     * This function will be used to set Time format in MySql server format.
     *
     * @param date $time
     * @return date $time
     * @author Daffodil
     */
    public static function setDbTime($time)
    {
        //Changing date format and return 0 incase og wrong date
        if (strtotime($time)) {
            $time = date(self::$dbTimeFormat, strtotime($time));
        } else {
            $time = '';
        }

        return $time;
    }

    /**
     * This function will be used to set Date format.
     *
     * @param date $date
     * @return date $date
     * @author Daffodil
     */
    public static function showDate($date = 0)
    {
        //Get Data from object
        if (is_object($date)) {
            $date = (string) $date;
        }

        //Changing date format and return 0 incase of wrong date
        if (strtotime($date) && $date != 0) {
            $date = date(self::$userDateFormat, strtotime($date));
        } else {
            $date = 'NA';
        }

        return $date;
    }

    /**
     * This function will be used to set Date & Time format.
     *
     * @param date $date
     * @return date $date
     * @author Daffodil
     */
    public static function showDateTime($date = 0)
    {

        //Get Data from object
        if (is_object($date)) {
            $date = (string) $date;
        }

        //Changing date format and return 0 incase of wrong date
        if (strtotime($date)) {
            $date = date(self::$userDateTimeFormat, strtotime($date));
        } else {
            $date = 'NA';
        }

        return $date;
    }
}
