<?php
namespace App\Tests\Admin\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Entity\ExchangeRate;

class ExchangeRateControllerTest extends WebTestCase
{
    /**
     * Test exchange rate listing function.
     */
    public function testIndex()
    {
        $client = static::createClient();

        $client->request('GET', '/admin/rates/');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    
    /**
     * Test exchange rate form view.
     */
    public function testAddExchangeRateView()
    {
        $client = static::createClient();

        $client->request('GET', '/admin/rates/new');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
    
    /**
     * Test display add exchange rate.
     */
    public function testAddExchangeRate()
    {
        $client = static::createClient();

        $client->request('POST', '/admin/rates/new', ['base_currency' => '',
                                                        'currency' => '',
                                                        'rate' => '',
                                                        'last_updated' => '', ]);

        //Check status
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
